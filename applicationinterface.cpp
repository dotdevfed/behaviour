#include "applicationinterface.h"
#include <QQmlEngine>
#include <QQmlContext>

ApplicationInterface::ApplicationInterface(QQmlContext * context, QObject * parent)
    : QObject(parent)
    , _currentExperiment("Default_Experiment")
    , _context(context)
{
    _context->setContextProperty("applicationInterface", this);
    _context->setContextProperty("currentExperiment", &_currentExperiment);


    _eventTypes.append("Single");
    _eventTypes.append("Toggle");
    _eventTypes.append("Keep Pressed");

    emit eventTypesChanged();
}

QVariantList ApplicationInterface::eventTypes()
{
    return _eventTypes;
}
