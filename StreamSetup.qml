import QtQuick 2.4
import QtQuick.Controls 2.1

Item {
    id: item1
    width: 1280
    height: 800
    property alias button: button

    Rectangle {
        id: rectangle
        width: 0
        height: 0
        color: "#607d8b"
        anchors.fill: parent

        ListView {
            id: listView
            x: 0
            y: 0
            width: 0
            height: 0
            spacing: 3
            anchors.left: parent.left
            anchors.leftMargin: 15
            antialiasing: true
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 70
            anchors.right: parent.right
            anchors.rightMargin: 15
            model: currentExperiment.eventStreams

            delegate: StreamListElement {
            }
        }

        Rectangle {
            id: rectangle1
            height: 40
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15

            Button {
                id: button
                x: 270
                width: 200
                text: qsTr("Add  Event Stream")
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                onClicked:  currentExperiment.addStream("Evento", 1)
            }
        }
    }
}
