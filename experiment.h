#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <QObject>
#include <QLinkedList>
#include <QMap>
#include <event.h>

class Experiment : public QObject {
    Q_OBJECT
    Q_PROPERTY(QVariantList eventStreams READ eventStreams NOTIFY eventStreamsChanged)

public:
    Experiment(QString);

    Q_INVOKABLE void addStream(QString name, qint8 type);
    Q_INVOKABLE void removeStream(int index);

    Q_INVOKABLE void setStreamName(int index, QString name);
    Q_INVOKABLE void setStreamType(int index, int type);

    Q_INVOKABLE void event(int index);

    Q_INVOKABLE void debugPrint();

    void singleEvent(qint32 code);
    void doubleEvent(qint32 code);

    void saveToJson();


    QVariantList eventStreams();

private:
    void addEvent(qint32, qint64);

    QString _name;
    QMap<qint32, QLinkedList<Event>> _data;
    QLinkedList<Event> _allData;
    QFile _mappedFile;
    QVariantList _eventStreams;

signals:
    void eventStreamsChanged();


};

#endif // EXPERIMENT_H
