import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
/*

500 #607D8B
50#ECEFF1
100#CFD8DC
200#B0BEC5
300#90A4AE
400#78909C
500#607D8B
600#546E7A
700#455A64
800#37474F
900#263238

*/
ApplicationWindow {
    visible: true
    width: 1280
    height: 800
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        StreamSetup
        {

        }

        Experiment
        {

        }


    }

    header: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {

            text: qsTr("Setup Experiment")
        }
        TabButton {
            text: qsTr("Experiment")
        }
        TabButton {
            text: qsTr("Settings")
        }
    }


    Rectangle {
        id: background
        color: "#607d8b"
        anchors.fill: parent
        z: -1
    }
}
