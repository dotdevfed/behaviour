import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.2

RowLayout {
  id: item1
  height: 40
  property alias streamName: streamName
  property alias comboBox: comboBox
  property alias removeButton: removeButton

  TextField {
    Layout.fillWidth: true
    id: streamName
    height: 40
    text: modelData.name
    anchors.verticalCenter: parent.verticalCenter
    onEditingFinished: currentExperiment.setStreamName(index, streamName.text)
  }

  ComboBox {
    model: applicationInterface.eventTypes
    currentIndex: modelData.type
    id: comboBox
    width: 200
    height: 40
   onCurrentTextChanged: currentExperiment.setStreamType(index, comboBox.currentIndex)
  }

  RoundButton {
    id: removeButton
    height: 40
    width: 40
    text: "-"
    font.bold: true
    onClicked: currentExperiment.removeStream(index)
  }
}
