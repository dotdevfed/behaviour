#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <applicationinterface.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    app.setOrganizationDomain("Westades Studios");
    app.setApplicationName("Behaviour Tracker");

    if (engine.rootObjects().isEmpty())
        return -1;

    ApplicationInterface applicationInterface(engine.rootContext(), &app);

    return app.exec();
}
