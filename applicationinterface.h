#ifndef APPLICATIONINTERFACE_H
#define APPLICATIONINTERFACE_H

#include <QtCore>
#include <QObject>
#include <QQmlContext>
#include <experiment.h>

class ApplicationInterface : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList eventTypes READ eventTypes NOTIFY eventTypesChanged)

public:
    ApplicationInterface( QQmlContext *, QObject * = nullptr );

    QVariantList eventTypes();

private:
    QQmlContext * _context;
    Experiment _currentExperiment;
    QVariantList _eventTypes;

signals:
    void eventTypesChanged();
};

#endif // APPLICATIONINTERFACE_H
