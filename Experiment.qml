import QtQuick 2.4

import QtQuick 2.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Item {
  width: 1280
  height: 800

  //property alias mouseArea1: mouseArea1
  // property alias mouseArea: mouseArea
  RowLayout {
    id: rowLayout
    spacing: 15
    anchors.rightMargin: 15
    anchors.leftMargin: 15
    anchors.bottomMargin: 15
    anchors.topMargin: 15
    anchors.fill: parent

    Rectangle {
      id: rectangle
      width: 200
      height: 200
      color: "#37474f"
      radius: 15
      Layout.fillWidth: true
      Layout.fillHeight: true

      ColumnLayout {
        id: columnLayout
        anchors.topMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        anchors.fill: parent

        Repeater {
          model: currentExperiment.eventStreams
          delegate: Rectangle {
            id: rectangle3
            color: "#607d8b"
            radius: 15
            border.width: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: index % 2 == 0

            Text {
              id: text1
              color: "#eceff1"
              text: modelData.name
              font.bold: false
              anchors.fill: parent
              verticalAlignment: Text.AlignVCenter
              horizontalAlignment: Text.AlignHCenter
              renderType: Text.NativeRendering
              font.pixelSize: 22
            }

            MouseArea {
              id: mouseArea
              anchors.fill: parent
              onPressed: currentExperiment.event(index);
              onReleased: {
                if (currentExperiment.eventStreams[index].type == 2)
                  currentExperiment.event(index)
              }
            }
          }
        }
      }
    }

    Rectangle {
      id: rectangle2
      width: 200
      height: 200
      color: "#37474f"
      radius: 15
      Layout.fillHeight: true
      Layout.fillWidth: true
      Column{
        Button
        {
        text: "PrintData"
          onPressed: currentExperiment.debugPrint();
        }

        Text {
          color: "#eceff1"
          text: "Data tarcking + share send export etc..?"
          font.bold: false
          anchors.fill: parent
          verticalAlignment: Text.AlignVCenter
          horizontalAlignment: Text.AlignHCenter
          renderType: Text.NativeRendering
          font.pixelSize: 22
        }
      }
    }

    Rectangle {
      id: rectangle5
      width: 200
      height: 200
      color: "#37474f"
      radius: 15
      Layout.fillWidth: true
      Layout.fillHeight: true

      ColumnLayout {
        id: columnLayout2
        anchors.topMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        anchors.fill: parent

        Repeater {
          model: currentExperiment.eventStreams
          delegate: Rectangle {
            id: rectangle4
            color: "#607d8b"
            radius: 15
            border.width: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: index % 2 == 1

            Text {
              id: text2
              color: "#eceff1"
              text: modelData.name
              font.bold: false
              anchors.fill: parent
              verticalAlignment: Text.AlignVCenter
              horizontalAlignment: Text.AlignHCenter
              renderType: Text.NativeRendering
              font.pixelSize: 22
            }

            MouseArea {
              id: mouseArea1
              anchors.fill: parent
              onPressed: currentExperiment.event(index);
              onReleased: {
                if (currentExperiment.eventStreams[index].type == 2)
                  currentExperiment.event(index)
              }
            }
          }
        }
      }
    }
  }
}
