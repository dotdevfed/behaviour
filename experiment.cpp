#include "experiment.h"
#include <QJsonDocument>
#include <QDir>

void Experiment::addEvent(qint32 c, qint64 t)
{
    QLinkedList<Event> dataList = _data.value(c);
    _data[c].push_back(Event(c, t));
    _allData.push_back(Event(c, t));

    qDebug() << c << " " << t;
}

Experiment::Experiment(QString name)
    : _name(name)
{
    auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    if (path.isEmpty()) {
        qFatal("Cannot determine settings storage location");
    }

    QDir d{path};

    if (d.mkpath(d.absolutePath()) && QDir::setCurrent(d.absolutePath())) {

        qDebug() << "File in" << QDir::currentPath();
        _mappedFile.setFileName(name + ".exp");

        if (!_mappedFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
            qFatal("Cannot Open Experiment file");
        }
    }
}

void Experiment::addStream(QString name, qint8 type)
{
    QVariantMap streamData;
    streamData.insert("id", _eventStreams.count());
    streamData.insert("name", name);
    streamData.insert("type", type);
    streamData.insert("blocked", false);

    _eventStreams.append(streamData);
    emit eventStreamsChanged();
}

void Experiment::removeStream(int index)
{
    _eventStreams.removeAt(index);
    emit eventStreamsChanged();

}

void Experiment::setStreamName(int index, QString name)
{

    QVariantMap m = _eventStreams[index].toMap();
    m.insert("name", name);
    _eventStreams[index] = m;
    emit eventStreamsChanged();
}

void Experiment::setStreamType(int index, int type)
{
    QVariantMap m = _eventStreams[index].toMap();
    m.insert("type", type);
    _eventStreams[index] = m;
    emit eventStreamsChanged();
}

void Experiment::event(int index)
{
    int type = _eventStreams.at(index).toMap()["type"].toInt();

    if (type == 0) {
        doubleEvent(index);
    }

    if (type == 1) {
        singleEvent(index);
    }

    if (type == 2) {
        singleEvent(index);

    }
}

void Experiment::debugPrint()
{
    for (QLinkedList<Event> eventStream : _data) {
        for (Event e : eventStream) {
            qDebug() << e.code() << " " << e.time();
        }
    }
}

void Experiment::singleEvent(qint32 index)
{
    qint64 time = QDateTime::currentMSecsSinceEpoch();
    addEvent(index, time);
}


void Experiment::doubleEvent(qint32 index)
{
    qint64 time = QDateTime::currentMSecsSinceEpoch();
    addEvent(index, time);
    addEvent(index, time);
}

void Experiment::saveToJson()
{
    QJsonDocument saveDocument;

}

QVariantList Experiment::eventStreams()
{
    return _eventStreams;
}
