import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: experimentPage
    Label {
        text: qsTr("Experiment")
        anchors.centerIn: parent
    }
    GridLayout {
        id: grid
        columns: 3
        height: 200
        width: 200
        Repeater {
            model: currentExperiment.eventStreams
            delegate: Button {
                height: 200
                width: 200
                onClicked: currentExperiment.event(modelData.type);
                Label {
                    text: qsTr("event" + modelData.name)
                    anchors.centerIn: parent
                }
            }
        }
    }
}
