#include "event.h"

Event::Event(qint32 code, qint64 time)
    : _code(code)
    , _time(time)
{

}

qint32 Event::code() const
{
    return _code;
}

qint64 Event::time() const
{
    return _time;
}
