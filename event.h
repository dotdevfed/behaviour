#ifndef EVENT_H
#define EVENT_H

#include <QtCore>

class Event {

public:
    Event(qint32 code, qint64 time);

    qint32 code() const;

    qint64 time() const;

private:
    qint32 _code = 0;
    qint64 _time = 0;

};

#endif // EVENT_H
